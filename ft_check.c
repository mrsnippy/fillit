/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/03 08:21:34 by nmatushe          #+#    #+#             */
/*   Updated: 2017/01/03 08:23:49 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int	ft_check(char ***fld, int *crd, int size)
{
	int y;
	int f;

	f = 0;
	y = 0;
	while (y < 7)
	{
		if (crd[y] < size && crd[y + 1] < size &&
			(*fld)[crd[y]][crd[y + 1]] == '.')
			f++;
		y += 2;
	}
	if (f == 4)
		return (1);
	return (0);
}
