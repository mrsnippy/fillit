/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_split.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/11 12:26:12 by dmurovts          #+#    #+#             */
/*   Updated: 2017/01/03 09:56:54 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int	ft_l_word_size(char *s, int i)
{
	int	k;

	k = 0;
	while (s[i])
	{
		if (s[i] == '\n' && (s[i + 1] == '\n' || !s[i + 1]))
			break ;
		k++;
		i++;
	}
	return (k + 1);
}

static int	ft_l_cnt(char *s)
{
	int	i;
	int j;

	i = 0;
	j = 0;
	while (s[i])
	{
		if (s[i] != '\n' && (s[i + 1] == '\n' || !s[i + 1])
			&& (s[i + 2] == '\n' || !s[i + 2]))
		{
			j++;
		}
		i++;
	}
	return (j);
}

static int	ft_l_pro(char *s, char ***res)
{
	if (!s)
		return (0);
	if (!(*res = (char **)malloc((ft_l_cnt((char *)s) + 1) * sizeof(char *))))
		return (0);
	return (1);
}

char		**ft_split_split(char *str)
{
	char	**res;
	int		i;
	int		j;
	int		size;

	size = 0;
	i = 0;
	j = 0;
	if (!ft_l_pro((char *)str, &res) || str[i] == '\n')
		return (NULL);
	while (str[i])
	{
		if (str[i] != '\n')
		{
			size = ft_l_word_size((char *)str, i);
			res[j] = ft_strsub(str, i, size);
			i += size;
			j++;
		}
		else
			i++;
	}
	res[j] = 0;
	return ((j <= 26) ? res : NULL);
}
