/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/11 13:49:32 by dmurovts          #+#    #+#             */
/*   Updated: 2017/01/03 12:03:06 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static char	*ft_read(char *name)
{
	char	*str;
	int		fd;
	int		size;
	int		rd;

	size = 21;
	while (size < 550)
	{
		str = ft_strnew(size);
		if ((fd = open(name, O_RDONLY)) == -1)
			return (NULL);
		if ((rd = read(fd, str, size)) == -1 || rd == 0)
			return (NULL);
		if (str[size - 1] != '\n')
			return (str);
		size += 21;
		free(str);
		if (close(fd) == -1)
			return (NULL);
	}
	return (NULL);
}

void		ft_print(char **out)
{
	int i;

	i = -1;
	while (out[++i])
	{
		ft_putstr(out[i]);
		ft_putchar('\n');
	}
}

static int	**ft_validate(char **maps, int **cords, int *i)
{
	char	**map;

	while (maps[*i])
	{
		if (!(map = ft_4split(maps[*i], '\n')))
		{
			ft_putstr("error\n");
			return (0);
		}
		if (!ft_validator(map))
		{
			ft_putstr("error\n");
			return (0);
		}
		if (!(cords[*i] = ft_cord(map, i)))
		{
			ft_putstr("error\n");
			return (0);
		}
		*i += 1;
	}
	return (cords);
}

static	int	ft_extra(char *str)
{
	int i;
	int len;
	int k;

	len = ft_strlen(str);
	i = -1;
	k = 0;
	while (str[++i])
	{
		if (str[i] == '\n')
			k++;
		if (k > 2)
			return (0);
		if (str[i] != '\n')
			k = 0;
	}
	if (str[len - 1] == '\n' && str[len - 2] == '\n')
		return (0);
	return (1);
}

int			main(int argc, char **argv)
{
	char	*read;
	char	**maps;
	int		**cords;
	int		i;
	int		res;

	if (argc != 2)
		return (0);
	i = 0;
	cords = (int **)malloc(sizeof(int *) * 27);
	if (!(read = ft_read(argv[1])) || !(ft_extra(read)))
	{
		ft_putstr("error\n");
		return (0);
	}
	if (!(maps = ft_split_split(read)))
	{
		ft_putstr("error\n");
		return (0);
	}
	if (!(cords = ft_validate(maps, cords, &i)))
		return (0);
	res = ft_algo(cords, i);
	return (0);
}
