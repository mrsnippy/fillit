/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/10 15:50:08 by dmurovts          #+#    #+#             */
/*   Updated: 2017/01/03 09:29:34 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H
# include <stdio.h>
# include <stdlib.h>
# include <fcntl.h>
# include <unistd.h>

void	ft_putstr(char const *s);
void	ft_putchar(char c);
char	**ft_split_split(char *str);
char	**ft_4split(char const *str, char c);
char	*ft_strsub(char const *s, unsigned int start, size_t len);
char	*ft_strnew(size_t size);
int		*ft_cord(char **arr, int *x);
int		ft_validator(char **map);
int		ft_fact(int a);
int		ft_algo(int **crds, int n);
void	ft_print(char **out);
int		**ft_swap(int **arr, int a, int b);
size_t	ft_strlen(char *inp);
char	**ft_field(int size);
int		ft_check(char ***fld, int *crd, int size);
int		*ft_move(int *arr, int i, int j);

#endif
