# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/12/06 14:02:06 by dmurovts          #+#    #+#              #
#    Updated: 2017/01/03 13:17:01 by dmurovts         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
CFLAGS = -Wall -Wextra -Werror
NAME = fillit
SRC = ft_4split.c ft_algo.c     ft_check.c   ft_cord.c         ft_fact.c   \
	  ft_move.c   ft_putchar.c  ft_putstr.c  ft_split_split.c  ft_strlen.c \
	  ft_strnew.c ft_strsub.c   ft_swap.c    ft_validator.c    ft_field.c  \
	  main.c

OBJS = $(SRC:.c=.o)

all: $(NAME)

%.o: %.c
	gcc $(CFLAGS) -c -o $@ $<

$(NAME): $(OBJS)
	$(CC) $(CFLAGS) -o $(NAME) $(OBJS)

clean:
	/bin/rm -f $(OBJS)

fclean: clean
	/bin/rm -f $(NAME)

re: fclean all

.PHONY: fclean clean re all
