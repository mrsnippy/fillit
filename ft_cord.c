/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cord.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/13 16:12:32 by dmurovts          #+#    #+#             */
/*   Updated: 2017/01/03 12:11:51 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int	ft_c_check(int **crd, char **arr)
{
	int		i;
	int		j;
	int		l;
	int		k;

	k = 0;
	l = 0;
	i = -1;
	while (arr[++i])
	{
		j = -1;
		while (arr[i][++j])
		{
			if (arr[i][j] == '#')
			{
				k++;
				(*crd)[l] = i;
				(*crd)[l + 1] = j;
				l += 2;
			}
		}
	}
	return (k);
}

int			*ft_cord(char **arr, int *x)
{
	int		*crd;
	int		k;

	crd = (int *)malloc(9 * sizeof(int));
	k = ft_c_check(&crd, arr);
	if (k != 4)
		return (NULL);
	crd[8] = *x;
	return (crd);
}
