/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_field.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/03 08:19:51 by nmatushe          #+#    #+#             */
/*   Updated: 2017/01/03 08:23:31 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	**ft_field(int size)
{
	char	**res;
	int		i;
	int		j;

	i = 0;
	res = (char **)malloc(sizeof(char *) * (size + 1));
	while (i < size)
	{
		res[i] = (char *)malloc(sizeof(char) * (size + 1));
		j = -1;
		while (++j < size)
			res[i][j] = '.';
		res[i][j] = '\0';
		i++;
	}
	res[i] = NULL;
	return (res);
}
