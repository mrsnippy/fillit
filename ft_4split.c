/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_4split.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/13 15:53:27 by dmurovts          #+#    #+#             */
/*   Updated: 2017/01/03 12:12:03 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int	ft_word_size(char *s, char c, int i)
{
	int	k;

	k = 0;
	while (s[i] != c && s[i])
	{
		k++;
		i++;
	}
	return (k);
}

static int	ft_cnt(char *s, char c)
{
	int	i;
	int j;

	i = 0;
	j = 0;
	while (s[i])
	{
		if (s[i] != c && (s[i + 1] == c || !s[i + 1]))
			j++;
		i++;
	}
	return (j);
}

static int	ft_pro(char *s, char c, char ***res)
{
	int a;

	if (!s)
		return (0);
	a = ft_cnt((char *)s, c);
	if (a != 4)
		return (0);
	if (!(*res = (char **)malloc((a + 1) * sizeof(char *))))
		return (0);
	return (1);
}

char		**ft_4split(char const *str, char c)
{
	char	**res;
	int		size;
	int		i;
	int		j;

	size = 0;
	i = 0;
	j = -1;
	if (!ft_pro((char *)str, c, &res))
		return (NULL);
	while (str[i])
	{
		if (str[i] != c)
		{
			size = ft_word_size((char *)str, c, i);
			if (size != 4)
				return (NULL);
			res[++j] = ft_strsub(str, i, size);
			i += size;
		}
		else
			i++;
	}
	res[++j] = 0;
	return (res);
}
