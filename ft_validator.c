/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_validator.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/18 20:44:45 by dmurovts          #+#    #+#             */
/*   Updated: 2017/01/03 12:12:24 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int	ft_validator(char **map)
{
	int	i;
	int	j;
	int	k;

	k = 0;
	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			if (map[i][j] == '#')
			{
				if ((i + 1) < 4)
					k = ((map[i + 1][j] == '#') ? k + 1 : k);
				if ((j + 1) < 4)
					k = ((map[i][j + 1] == '#') ? k + 1 : k);
			}
			if (map[i][j] != '.' && map[i][j] != '#')
				return (0);
		}
	}
	if (k == 3 || k == 4)
		return (1);
	return (0);
}
