/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/03 09:28:53 by nmatushe          #+#    #+#             */
/*   Updated: 2017/01/03 09:30:27 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int	*ft_move(int *arr, int i, int j)
{
	int k;
	int	*res;

	res = (int *)malloc(sizeof(int) * 9);
	k = -1;
	while (++k <= 7)
	{
		if (k % 2 == 0)
			res[k] = arr[k] - arr[0] + i;
		else
			res[k] = arr[k] - arr[1] + j;
	}
	res[8] = arr[8];
	return (res);
}
