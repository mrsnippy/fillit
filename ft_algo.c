/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_algo.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/29 10:23:52 by dmurovts          #+#    #+#             */
/*   Updated: 2017/01/03 09:34:50 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void	ft_insert(char ***fld, int *crd)
{
	int	y;

	y = 0;
	while (y < 7)
	{
		(*fld)[crd[y]][crd[y + 1]] = crd[8] + 'A';
		y += 2;
	}
}

static int	ft_sqr(char ***fld, int *crd, int i, int j)
{
	int	size;

	size = ft_strlen(*fld[0]);
	while ((*fld)[i])
	{
		while ((*fld)[i][j])
		{
			if ((*fld)[i][j] == '.')
			{
				crd = ft_move(crd, i, j);
				if (ft_check(fld, crd, size))
				{
					ft_insert(fld, crd);
					return (1);
				}
			}
			j++;
		}
		j = 0;
		i++;
	}
	return (0);
}

static int	*ft_clean(char ***fld, int *crd)
{
	int i;
	int j;
	int	*k;

	k = (int *)malloc(sizeof(int) * 3);
	k[0] = 0;
	k[1] = 0;
	k[2] = 0;
	i = -1;
	while ((*fld)[++i])
	{
		j = -1;
		while ((*fld)[i][++j])
			if ((*fld)[i][j] == (crd[8] + 'A'))
			{
				k[1] = (k[0] == 0) ? i : k[1];
				k[2] = (k[0] == 0) ? j + 1 : k[2];
				k[0] += 1;
				(*fld)[i][j] = '.';
			}
	}
	return (k);
}

static int	ft_fillit(char ***fld, int **crd, int *xz, int *k)
{
	int i;
	int	j;

	i = 0;
	j = 0;
	while (xz[0] < xz[1])
	{
		if (!(ft_sqr(fld, crd[xz[0]], k[1], k[2])))
			return (0);
		else
		{
			xz[0] += 1;
			k[1] = 0;
			k[2] = 0;
			if (ft_fillit(fld, crd, xz, k))
				return (1);
			xz[0] -= 1;
			k = ft_clean(fld, crd[xz[0]]);
		}
	}
	ft_print(*fld);
	return (1);
}

int			ft_algo(int **crds, int n)
{
	int		size;
	int		stop;
	char	**fld;
	int		*k;
	int		*xn;

	size = 2;
	k = (int *)malloc(sizeof(int) * 3);
	xn = (int *)malloc(sizeof(int) * 2);
	k[0] = 0;
	k[1] = 0;
	k[2] = 0;
	xn[0] = 0;
	xn[1] = n;
	while (size++)
	{
		if ((n * 4) <= size * size)
		{
			fld = ft_field(size);
			stop = ft_fillit(&fld, crds, xn, k);
			if (stop == 1)
				return (0);
		}
	}
	return (0);
}
